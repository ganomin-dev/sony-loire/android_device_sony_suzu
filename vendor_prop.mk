# Audio
PRODUCT_PROPERTY_OVERRIDES += \
    audio.deep_buffer.media=true \
    audio.dolby.ds2.enabled=true \
    audio.offload.buffer.size.kb=64 \
    audio.offload.gapless.enabled=true \
    audio.offload.min.duration.secs=30 \
    audio.offload.multiple.enabled=false \
    audio.offload.pcm.16bit.enable=true \
    audio.offload.pcm.24bit.enable=true \
    audio.offload.track.enable=true \
    audio.offload.video=false \
    audio.playback.mch.downsample=true \
    audio.pp.asphere.enabled=false \
    audio.safx.pbe.enabled=true \
    av.debug.disable.pers.cache=true \
    persist.audio.fluence.speaker=true \
    persist.audio.fluence.voicecall=true \
    persist.audio.fluence.voicerec=false \
    persist.bluetooth.avrcpversion=avrcp16 \
    persist.vendor.radio.add_power_save=1 \
    ro.media.av_latency=60 \
    ro.qc.sdk.audio.fluencetype=none \
    ro.qc.sdk.audio.ssr=false \
    ro.qti.sensors.gravity=true \
    ro.somc.ldac.audio.supported=true \
    tunnel.audio.encode = false \
    use.voice.path.for.pcm.voip=true \
    vendor.audio.hw.aac.encoder=true \
    vendor.audio.parser.ip.buffer.size=262144 \
    voice.conc.fallbackpath=deep-buffer \
    voice.playback.conc.disabled=true \
    voice.record.conc.disabled=false \
    voice.voip.conc.disabled=false \
# Bluetooth
PRODUCT_PROPERTY_OVERRIDES += \
    bluetooth.hfp.client=1 \
    ro.qti.sensors.bte=false \
# CNE
PRODUCT_PROPERTY_OVERRIDES += \
    persist.cne.feature=1 \
    persist.cne.rat.wlan.chip.oem=nqc \
# Camera
PRODUCT_PROPERTY_OVERRIDES += \
    camera.display.lmax=1280x720 \
    camera.display.umax=1920x1080 \
    camera.hal1.packagelist=com.skype.raider,com.google.android.talk \
    camera.lowpower.record.enable=1 \
    media.camera.ts.monotonic=1 \
# Charging
PRODUCT_PROPERTY_OVERRIDES += \
    ro.cutoff_voltage_mv=3200 \
# DPM
PRODUCT_PROPERTY_OVERRIDES += \
    persist.vendor.dpm.feature=1 \
# FM
PRODUCT_PROPERTY_OVERRIDES += \
    ro.fm.transmitter=false \
# FRP
PRODUCT_PROPERTY_OVERRIDES += \
    ro.frp.pst=/dev/block/bootdevice/by-name/config \
# FUSE
PRODUCT_PROPERTY_OVERRIDES += \
    persist.fuse_sdcard=true \
# Graphics
PRODUCT_PROPERTY_OVERRIDES += \
    debug.egl.hw=0 \
    debug.sf.enable_hwc_vds=1 \
    debug.sf.hw=0 \
    debug.sf.latch_unsignaled=1 \
    debug.sf.recomputecrop=0 \
    dev.pm.dyn_samplingrate=1 \
    persist.demo.hdmirotationlock=false \
    ro.opengles.version=196610 \
# Media
PRODUCT_PROPERTY_OVERRIDES += \
    media.aac_51_output_enabled=true \
    media.msm8956hw=0 \
    media.settings.xml=/vendor/etc/media_profiles_vendor.xml \
    mm.enable.qcom_parser=760765 \
    mm.enable.smoothstreaming=true \
    mmp.enable.3g2=true \
    ro.qualcomm.cabl=0 \
    vidc.debug.perf.mode=2 \
    vidc.dec.downscalar_height=1088 \
    vidc.dec.downscalar_width=1920 \
    vidc.enc.dcvs.extra-buff-count=2 \
    vidc.enc.narrow.searchrange=1 \
# Misc
PRODUCT_PROPERTY_OVERRIDES += \
    db.log.slow_query_threshold=100 \
    debug.enable.sglscale=1 \
    debug.mdpcomp.logs=0 \
    persist.debug.8976.config=true \
    persist.debug.coresight.config=none \
    persist.hwc.downscale_threshold=1.15 \
    persist.hwc.enable_vds=1 \
    persist.hwc.mdpcomp.enable=true \
    persist.ims.disableUserAgent=1 \
    persist.keyprovd.attest.prov=1 \
    persist.keyprovd.attest.version=0 \
    persist.keyprovd.fido.provision=1 \
    persist.keyprovd.fido.version=0 \
    persist.net.doxlat=true \
    persist.qfp=false \
    persist.rcs.supported=0 \
    persist.speaker.prot.enable=true \
    persist.sys.touch.glove_mode=0 \
    persist.vendor.ims.vcel_rtcp_report=5 \
    persist.zawgyi.selected=false \
    qemu.hw.mainkeys=0 \
    ro.board.platform=msm8952 \
    ro.com.android.prov_mobiledata=false \
    ro.product.board=msm8952 \
    ro.product.locale.excluded=ar_EG \
    ar_IL \
    fa_IR \
    ro.qcom.ad.calib.data=/system/etc/ad_calib.cfg \
    ro.qcom.ad=1 \
    ro.qfusion_use_report_period=false \
    ro.sdcrypt.supported=true \
    ro.sys.sdcardfs=1 \
    sched.colocate.enable=1 \
    sony.ahc.supported=yes \
    sony.effect.custom.caplus_hs=0x298 \
    sony.effect.custom.caplus_sp=0x2B8 \
    sony.effect.custom.sp_bundle=0x122 \
    sony.effect.use.proxy=true \
    sys.backlight_on=1 \
    sys.cover_state=0 \
    sys.hwc_disable_hdr=1 \
# NFC
PRODUCT_PROPERTY_OVERRIDES += \
    ro.nfc.icon.enable=false \
    ro.nfc.on.default=true \
    ro.nfc.se.sim.enable=true \
    ro.nfc.se.smx.enable=false \
# Netmgr
PRODUCT_PROPERTY_OVERRIDES += \
    persist.data.mode=concurrent \
    persist.data.netmgrd.qos.enable=true \
    ro.use_data_netmgrd=true \
# Perf
PRODUCT_PROPERTY_OVERRIDES += \
# QTI
PRODUCT_PROPERTY_OVERRIDES += \
    persist.vendor.qti.telephony.vt_cam_interface=1 \
    ro.qti.sdk.sensors.gestures=false \
    ro.qti.sensors.amd=false \
    ro.qti.sensors.cmc=false \
    ro.qti.sensors.dev_ori=false \
    ro.qti.sensors.dpc=false \
    ro.qti.sensors.facing=false \
    ro.qti.sensors.fast_amd=false \
    ro.qti.sensors.fns=false \
    ro.qti.sensors.game_rv=true \
    ro.qti.sensors.georv=true \
    ro.qti.sensors.gtap=false \
    ro.qti.sensors.iod=false \
    ro.qti.sensors.laccel=true \
    ro.qti.sensors.mot_detect=false \
    ro.qti.sensors.orientation=true \
    ro.qti.sensors.pam=false \
    ro.qti.sensors.pedometer=false \
    ro.qti.sensors.pmd=false \
    ro.qti.sensors.proximity=true \
    ro.qti.sensors.pug=false \
    ro.qti.sensors.qheart=false \
    ro.qti.sensors.qmd=false \
    ro.qti.sensors.rmd=false \
    ro.qti.sensors.rotvec=true \
    ro.qti.sensors.scrn_ortn=false \
    ro.qti.sensors.smd=true \
    ro.qti.sensors.sta_detect=false \
    ro.qti.sensors.step_counter=true \
    ro.qti.sensors.step_detector=true \
    ro.qti.sensors.tap=false \
    ro.qti.sensors.tilt=false \
    ro.qti.sensors.tilt_detector=true \
    ro.qti.sensors.vmd=false \
    ro.qti.sensors.wrist_tilt=false \
    ro.qti.sensors.wu=true \
    ro.vendor.at_library=libqti-at.so \
    ro.vendor.gt_library=libqti-gt.so \
    ro.vendor.qti.config.zram=true \
    ro.vendor.qti.core_ctl_max_cpu=4 \
    ro.vendor.qti.core_ctl_min_cpu=2 \
    ro.vendor.qti.sys.fw.empty_app_percent=50 \
    ro.vendor.qti.sys.fw.trim_cache_percent=100 \
    ro.vendor.qti.sys.fw.trim_empty_percent=100 \
    ro.vendor.qti.sys.fw.trim_enable_memory=2147483648 \
    ro.vendor.qti.sys.fw.use_trim_settings=true \
# Radio
PRODUCT_PROPERTY_OVERRIDES += \
    DEVICE_PROVISIONED=1 \
    persist.radio.VT_CAM_INTERFACE=1 \
    persist.radio.block_allow_data=1 \
    persist.radio.lw_enabled=true \
    persist.radio.redir_party_num=0 \
    persist.vendor.radio.mt_sms_ack=19 \
    persist.vendor.radio.oem_socket=true \
    persist.vendor.radio.wait_for_pbm=1 \
    ril.subscription.types=NV,RUIM \
    rild.libargs=-d /dev/smd0 \
    rild.libpath=/vendor/lib64/libril-qc-qmi-1.so \
    ro.com.android.dataroaming=false \
    telephony.lteOnCdmaDevice=0 \
# Sensors
PRODUCT_PROPERTY_OVERRIDES += \
# Time
PRODUCT_PROPERTY_OVERRIDES += \
    persist.timed.enable=true \
# WFD
PRODUCT_PROPERTY_OVERRIDES += \
    persist.debug.wfd.enable=1 \
# WLAN
PRODUCT_PROPERTY_OVERRIDES += \
# ZRAM
PRODUCT_PROPERTY_OVERRIDES += \
